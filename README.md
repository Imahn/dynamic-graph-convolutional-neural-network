# Dynamic Graph Convolutional Neural Network

The project was first on GitHub. Since I moved away from GitHub, I'm now re-uploading the files on GitLab. 

## Aim of the Project

In particle physics, we have a lot of proton-proton interactions per second. To analyze the events for anomalies to the Standard Model of particle physics, machine learning tools can be used. 

In this work, I'm applying a dynamic graph convolutional neural network (DGCNN) to Monte Carlo-simulated data to search for anomalies. Concretely, the anomalies I studies come from long-lived particles decaying into bottom quarks. I tried to find the signature of these anomalies when the bottom quarks decay in the tracker of the [CMS detector](https://cms.cern/detector). 

## Dynamic Graph Convolutional Neural Network

The motivation for creating a graph is the following: in tracking, the detector hits naturally form graphs. Thus, for each detector hit, we can create a k-nearest neighbors (k-NN) graph. However, this graph is **not** static, but as the name suggests, updates itself during training. This is done by using the edge convolution ("EdgeConv") operation, first proposed by M. Bronstein et al. in [arXiv:1801.07829](https://arxiv.org/pdf/1801.07829.pdf). 

The idea of EdgeConv is shown in the following Fig., taken from the above-mentioned paper: every 
vertex and edge of the graph $`G`$ has features. 

![edge_conv_idea.png](Images/edge_conv_idea.png)

The edge features are defined by 
```math
e_{ij} := h_{\mathbf{\Theta}}\left(\mathbf x_i, \mathbf x_j\right), 
```
where $`h_{\mathbf{\Theta}}`$ is a neural network with parameters $`\mathbf{\Theta}`$. 

At vertex $`i`$, the EdgeConv operation can be written as 
```math
x_{i}^{'} = \square_{j:(i, j)\in\mathcal{E}}h_{\mathbf{\Theta}}\left(\mathbf x_i, \mathbf x_j\right)
```
Here, $`\mathcal{E}`$ is the set of edges, $`i`$ is the central vertex, and $`j`$ refers to the k-NN of the 
central vertex $`i`$, and the aggregation is over the k-NN. The specific choices for 
$`h_{\mathbf{\Theta}}`$ and $`\square`$ can be considered as hyperparameters, and searching for 
the “right” choice for the given problem corresponds to hyperparameter tuning.

For $`h_{\mathbf{\Theta}}`$, a neural network with BatchNorm and Conv2D layers is used.

## Support
In case of questions/bugs, you can file an issue or an PR. 


