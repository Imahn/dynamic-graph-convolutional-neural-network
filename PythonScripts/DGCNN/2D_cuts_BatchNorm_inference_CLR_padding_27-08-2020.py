#!/usr/bin/env python
# coding: utf-8

# From now on, training = True will always be used for BatchNorm-layers, due to the superb performance of the val- and test-losses. 
# In this implementation, I will try CLR with stepsize = 10, and I will train for 81 epochs, i.e. for four full cycles. 
# In this implementation, I also shuffle the test-dataset. 
# Also, no dropout is used for the FCN, as the combination of Dropout and BatchNorm 
# is in general a bit complicated. 

import numpy as np, awkward, logging, tensorflow as tf, os, matplotlib.pyplot as plt, pickle, tensorflow, datetime, tensorflow_addons as tfa
from tensorflow import keras
# ToDo: Get right file. 
from tf_model_no_dropout_FCN_27_08_2020 import get_particle_net, get_particle_net_lite
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s')
# print(tf.config.list_physical_devices('GPU'))

from tensorflow.python.client import device_lib
print(device_lib.list_local_devices(), "\n")
print(datetime.datetime.now())

def stack_arrays(a, keys, axis=-1):
    flat_arr = np.stack([a[k].flatten() for k in keys], axis=axis)
    return awkward.JaggedArray.fromcounts(a[keys[0]].counts, flat_arr)

def pad_array(a, maxlen, value=0., dtype='float32'):
    x = (np.ones((len(a), maxlen)) * value).astype(dtype)
    for idx, s in enumerate(a):
        if not len(s):
            continue
        trunc = s[:maxlen].astype(dtype)
        x[idx, :len(trunc)] = trunc
    return x

class Dataset(object):
    # ToDo: Choose correct label! 
    def __init__(self, filepath, feature_dict = {}, label='cut_vtx_position', pad_len=1500, 
                 data_format='channel_first'):
        self.filepath = filepath
        self.feature_dict = feature_dict
        if len(feature_dict)==0:
            # ToDo (following next three lines): 
            feature_dict['points'] = ['hit_etaRel', 'hit_phiRel']
            feature_dict['features'] = ['hit_x', 'hit_y', 'hit_z']
            feature_dict['mask'] = ['hit_isStrip']
        self.label = label
        self.pad_len = pad_len
        assert data_format in ('channel_first', 'channel_last')
        self.stack_axis = 1 if data_format=='channel_first' else -1
        self._values = {}
        self._label = None
        self._load()

    def _load(self):
        logging.info('Start loading file %s' % self.filepath)
        counts = None
        with awkward.load(self.filepath) as a:
            self._label = a[self.label]
            for k in self.feature_dict:
                cols = self.feature_dict[k]
                if not isinstance(cols, (list, tuple)):
                    cols = [cols]
                arrs = []
                for col in cols:
                    if counts is None:
                        counts = a[col].counts
                    else:
                        assert np.array_equal(counts, a[col].counts)
                    arrs.append(pad_array(a[col], self.pad_len, -999.))
                self._values[k] = np.stack(arrs, axis=self.stack_axis)
        logging.info('Finished loading file %s' % self.filepath)

    def __len__(self):
        return len(self._label)

    def __getitem__(self, key):
        if key==self.label:
            return self._label
        else:
            return self._values[key]
    
    @property
    def X(self):
        return self._values
    
    @property
    def y(self):
        return self._label

    def shuffle(self, seed=None):
        if seed is not None:
            np.random.seed(seed)
        shuffle_indices = np.arange(self.__len__())
        np.random.shuffle(shuffle_indices)
        for k in self._values:
            self._values[k] = self._values[k][shuffle_indices]
        self._label = self._label[shuffle_indices]

# ToDo: Load files with 2D-cuts and etaRel, phiRel respectively. 
srcDirDataset = '/nfs/dust/cms/user/shekhzai/ParticleNet-LLP-fork/tf-keras/RegressionProblemWithKarla/converted/correctNormalizationAndCorrectNumberOfInputFeatures/'   
print("\n\nsrcDirDataset:", srcDirDataset)

train_dataset = Dataset(srcDirDataset + 'train_2D_cuts_etaRelAndphiRel.awkd', data_format='channel_last')
val_dataset = Dataset(srcDirDataset + 'val_2D_cuts_etaRelAndphiRel.awkd', data_format='channel_last')
test_dataset = Dataset(srcDirDataset + 'test_2D_cuts_etaRelAndphiRel.awkd', data_format='channel_last')

# ToDo:
model_type = 'particle_net' # choose between 'particle_net' and 'particle_net_lite'
print("\n", model_type)
# better: num_coordinates instead of num_classes
num_classes = train_dataset.y.shape[1]
input_shapes = {k:train_dataset[k].shape[1:] for k in train_dataset.X}
if 'lite' in model_type:
    model = get_particle_net_lite(num_classes, input_shapes)
else:
    model = get_particle_net(num_classes, input_shapes)


lr_schedule = tfa.optimizers.CyclicalLearningRate(
                                                  initial_learning_rate = 5e-6, 
                                                  maximal_learning_rate = 1e-4, 
                                                  step_size = 10, 
                                                  scale_mode = "cycle", 
                                                  scale_fn = lambda x: 1.0
                                                  )

# for getting the lr displayed like a metric
def get_lr_metric(optimizer): 
    def lr(y_true, y_pred): 
        return optimizer.lr
    return lr

# ToDo: Learning rate (when using TensorFlow Addons' Cyclical Learning Rate Function, I don't need to pass a learning rate.)
optimizer = keras.optimizers.Adam()
lr_metric = get_lr_metric(optimizer)

model.compile(loss='mse',
              optimizer = optimizer,
              metrics = [keras.metrics.RootMeanSquaredError(), lr_metric] 
              )
model.summary()

# ToDo: change srcDir
srcDir = '/nfs/dust/cms/user/shekhzai/ParticleNet-LLP-fork/tf-keras/RegressionProblemWithKarla/refurbedTrainings/GenVerticesUnprocessed/PNet/2D_cuts_etaRelAndphiRel_BatchNorm_inference_CLR_25-27-08-2020/four_cycles_padding_26-27_08_2020/'
save_dir = srcDir+'model_checkpoints/'
model_name = '%s_model.{epoch:03d}.h5' % model_type
if not os.path.isdir(save_dir):
    os.makedirs(save_dir)
filepath = os.path.join(save_dir, model_name)

checkpoint = keras.callbacks.ModelCheckpoint(
                                            filepath=filepath, 
                                            monitor='val_root_mean_squared_error', 
                                            verbose=1, 
                                            mode = 'min', 
                                            save_best_only=False, 
                                            save_weights_only = True)

progress_bar = keras.callbacks.ProgbarLogger()
scheduler = keras.callbacks.LearningRateScheduler(lr_schedule)
callbacks = [checkpoint, progress_bar, scheduler]

train_dataset.shuffle()
val_dataset.shuffle()
# ToDo: 
test_dataset.shuffle()

batch_size = 72
# ToDo: 
epochs = 45

print("batch_size:", batch_size)
print("epochs:", epochs)

histObj = model.fit(train_dataset.X, 
                    train_dataset.y, 
                    batch_size=batch_size, 
                    epochs=epochs, 
                    validation_data=(val_dataset.X, val_dataset.y),
                    shuffle=True, callbacks=callbacks
                    )

# Save your model at the end of the training
model_label = 'refurbed'
output_file = 'PNET_'+model_label+'.h5'
model.save(save_dir+output_file)
print("Model saved in ", save_dir+output_file)


print("\n\nmodel.evaluate(train_dataset.X, train_dataset.y):", model.evaluate(train_dataset.X, train_dataset.y, batch_size = batch_size))
print("\nmodel.evaluate(test_dataset.X, test_dataset.y):", model.evaluate(test_dataset.X, test_dataset.y, batch_size = batch_size))

# ToDo: Create the txt-file in the srcDir. 
text_file = open(srcDir + "evaluationPrintOuts.txt", "w")
text_file.write("model.evaluate(train_dataset.X, train_dataset.y): %s" % model.evaluate(train_dataset.X, train_dataset.y, batch_size = batch_size))
text_file.write("\n")
text_file.write("model.evaluate(test_dataset.X, test_dataset.y): %s" % model.evaluate(test_dataset.X, test_dataset.y, batch_size = batch_size))
text_file.close()

import numpy as np
import matplotlib.pyplot as plt

def plotLearningCurves(*histObjs):
    """This function processes all histories given in the tuple.
    Left losses, right RMSE
    """
    # too many plots
    if len(histObjs)>10: 
        print('Too many objects!')
        return
    # missing names
    for histObj in histObjs:
        if not hasattr(histObj, 'name'): histObj.name='?'
    names=[]
    # loss plot
    plt.rcParams.update({'font.size': 11.6}) #Larger font size
    ax = plt.subplot(1,1,1)
    # loop through arguments
    for histObj in histObjs:
        plt.plot(histObj.history['loss'])
        names.append('train '+histObj.name)
        plt.plot(histObj.history['val_loss'])
        names.append('validation '+histObj.name)
    plt.title('Loss (= MSE)')
    plt.xlabel('Epoch')
    plt.legend(names, loc='upper right')
    plt.grid(b = True)    

    plt.savefig(srcDir + "loss.pdf")
    plt.savefig(srcDir + "loss.png")
    
    with open(srcDir + "loss.pickle", "wb") as fid: 
        pickle.dump(ax, fid) 
    plt.close("all")
    
    # metrics plot
    plt.rcParams.update({'font.size': 11.6}) #Larger font size
    ax = plt.subplot(1,1,1)
    for histObj in histObjs:
        plt.plot(histObj.history['root_mean_squared_error'])
        plt.plot(histObj.history['val_root_mean_squared_error'])
    plt.title('Metrics (= RMSE) [cm]')
    plt.xlabel('Epoch')
    plt.legend(names, loc='upper right')
    plt.grid(b = True)
    
    plt.savefig(srcDir + "rmse.pdf")
    plt.savefig(srcDir + "rmse.png")
    
    with open(srcDir + "rmse.pickle", "wb") as fid: 
        pickle.dump(ax, fid)  
    plt.close("all")
    
    return plt


histObj.name=''
plot = plotLearningCurves(histObj)